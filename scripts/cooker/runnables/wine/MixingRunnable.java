package scripts.cooker.runnables.wine;

import static scripts.cooker.data.Constants.GRAPES;
import static scripts.cooker.data.Constants.JUG_OF_WATER;

import scripts.api.ABCUtil;
import scripts.api.Inventory;
import scripts.api.Player;
import scripts.api.Timing;

public class MixingRunnable implements Runnable {

    @Override
    public String toString() {
        return "Mixing";
    }

    @Override
    public void run() {
        if (Timing.waitCondition(() -> !Player.isAnimating() && timedActions(), 3000)) {
            Timing.waitCondition(() -> Player.isAnimating() || !Inventory.containsAll(GRAPES, JUG_OF_WATER), 1500);
        }
    }

    private boolean timedActions() {
        ABCUtil.performXPCheck();
        ABCUtil.performCheckTabs();
        ABCUtil.performExamineEntity();
        ABCUtil.performMoveMouse();
        ABCUtil.performRightClick();
        ABCUtil.performLeaveGame();
        return true;
    }
}
