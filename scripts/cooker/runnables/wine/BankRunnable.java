package scripts.cooker.runnables.wine;

import static scripts.cooker.data.Constants.GRAPES;
import static scripts.cooker.data.Constants.JUG_OF_WATER;
import static scripts.cooker.data.Constants.JUG_OF_WINE;
import static scripts.cooker.data.Constants.UNFERMENTED_WINE;

import scripts.api.Banking;
import scripts.api.Inventory;
import scripts.api.Timing;
import scripts.api.util.Logging;
import scripts.cooker.data.Vars;

public class BankRunnable implements Runnable {

    @Override
    public String toString() {
        return "Banking";
    }

    @Override
    public void run() {
        if (!Banking.isBankScreenOpen() && Banking.openBank(true)) {
            Timing.waitCondition(Banking::isBankLoaded, 1200);
        }
        if (Banking.isBankScreenOpen()) {
            handleBanking();
        }
    }

    private void handleBanking() {
        if (Inventory.contains(JUG_OF_WINE, UNFERMENTED_WINE)) {
            depositWine();
        }
        if (!Inventory.contains(JUG_OF_WINE, UNFERMENTED_WINE)) {
            if (!Inventory.contains(GRAPES)) {
                withdrawGrapes();
            }
            if (!Inventory.contains(JUG_OF_WATER)) {
                withdrawJugOfWater();
            }
        }
    }

    private void depositWine() {
        if (Banking.depositAll(JUG_OF_WINE, UNFERMENTED_WINE)) {
            Timing.waitCondition(() -> !Inventory.contains(JUG_OF_WINE, UNFERMENTED_WINE), 1200);
        }
    }

    private void withdrawGrapes() {
        int amount = Inventory.getCount(JUG_OF_WATER) == 14 ? 0 : 14;
        if (Banking.withdraw(amount, GRAPES)) {
            Timing.waitCondition(() -> Inventory.contains(GRAPES), 1200);
        } else if (!Banking.contains(GRAPES) && !Timing.waitCondition(() -> Banking.contains(GRAPES), 1200)) {
            Logging.critical("Out of grapes.");
            Vars.get().script.shutdown();
        }
    }

    private void withdrawJugOfWater() {
        int amount = Inventory.getCount(GRAPES) == 14 ? 0 : 14;
        if (Banking.withdraw(amount, JUG_OF_WATER)) {
            Timing.waitCondition(() -> Inventory.contains(JUG_OF_WATER), 1200);
        } else if (!Banking.contains(JUG_OF_WATER) && !Timing.waitCondition(() -> Banking.contains(JUG_OF_WATER), 1200)) {
            Logging.critical("Out of jugs of water.");
            Vars.get().script.shutdown();
        }
    }

}
