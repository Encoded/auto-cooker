package scripts.cooker.runnables;

import static scripts.api.ext.Constants.Interface.Parent.COOKING;
import static scripts.cooker.data.Constants.RAW_KARAMBWAN;

import org.tribot.api2007.Game;
import org.tribot.api2007.types.RSInterface;
import org.tribot.api2007.types.RSItem;
import org.tribot.api2007.types.RSObject;
import scripts.api.ABCUtil;
import scripts.api.Banking;
import scripts.api.Interact;
import scripts.api.Interfaces;
import scripts.api.Inventory;
import scripts.api.PathFinding;
import scripts.api.Player;
import scripts.api.Timing;
import scripts.cooker.data.Vars;

public class CookRunnable implements Runnable {

    @Override
    public String toString() {
        return "Attempting to Cook";
    }

    @Override
    public void run() {
        if (Banking.isBankScreenOpen() && Banking.close(false)) {
            Timing.waitCondition(() -> !Banking.isBankScreenOpen(), 1200);
        }
        if (!Banking.isBankScreenOpen()) {
            if (!Interfaces.isValid(COOKING)) {
                if (useRaw()) {
                    clickHeatSource();
                }
            }
            if (Interfaces.isValid(COOKING)) {
                cookAll();
            }
        }
    }

    private boolean useRaw() {
        if (!Inventory.isItemSelected()) {
            RSItem raw = Inventory.find().idEquals(Vars.get().rawId).getFirst();
            if (Interact.with(raw).click("Use")) {
                Timing.waitCondition(Inventory::isItemSelected, 900);
            }
        }
        return Inventory.isItemSelected();
    }

    private void clickHeatSource() {
        RSObject heatSource = Vars.get().location.getHeatSource();
        int multiplier = Game.isRunOn() && Game.getRunEnergy() >= 15 ? 400 : 600;
        int distance = PathFinding.distanceTo(heatSource, false);
        if (Interact.with(heatSource).walkTo().cameraTurn().click("Use")) {
            Timing.waitCondition(() -> Interfaces.isValid(COOKING), distance * multiplier + 1200);
        }
    }

    private void cookAll() {
        int child = 14;
        if (Vars.get().rawId == RAW_KARAMBWAN) {
            child = 15;
        }
        RSInterface cookAllWidget = Interfaces.find().parentEquals(COOKING).childEquals(child).getFirst();
        if (Interact.with(cookAllWidget).click("Cook") && Timing.waitCondition(Player::isAnimating, 1800)) {
            ABCUtil.generateTrackers(Inventory.getCount(Vars.get().rawId) * 2400, true);
        }
    }

}
