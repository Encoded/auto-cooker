package scripts.cooker.runnables.alkharid;

import static scripts.cooker.data.Constants.AL_KHARID_BANK_TILE;
import static scripts.cooker.data.Constants.AL_KHARID_CLOSED_DOOR_TILE;
import static scripts.cooker.data.Constants.AL_KHARID_OPEN_DOOR_TILE;

import org.tribot.api2007.types.RSObject;
import scripts.api.ABCUtil;
import scripts.api.Banking.Bank;
import scripts.api.Objects;
import scripts.api.PathFinding;
import scripts.api.Player;
import scripts.api.Timing;
import scripts.api.Walking;
import scripts.api.ext.Doors;

public class WalkBankRunnable implements Runnable {

    @Override
    public String toString() {
        return "Walking to Bank";
    }

    @Override
    public void run() {
        if (PathFinding.canReach(AL_KHARID_OPEN_DOOR_TILE)) {
            walkToBank();
        } else {
            openDoor();
        }
    }
    private void openDoor() {
        RSObject door = Objects.find().actionEquals("Open").atPosition(AL_KHARID_CLOSED_DOOR_TILE).getFirst();
        Doors.open(door);
    }

    private void walkToBank() {
        ABCUtil.performRunActivation();
        if (Walking.walkStraightPath(AL_KHARID_BANK_TILE)) {
            Timing.waitCondition(() -> Bank.AL_KHARID.contains(Player.getPosition()), 1200);
        }
    }

}
